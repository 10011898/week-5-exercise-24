﻿using System;

namespace week_5_exercise_24
{
    class Program
    {
        public static void NameWelcome(string name)
        {
            Console.WriteLine($"Hello {name}");
        }
        public static void Welcome()
        {
            Console.WriteLine("My First Method!");
        }
        public static string NameWelcome2(string name2)
        {
            return $"Hello again, {name2}";
        }
        public static string Multiply(int one, int two, int three)
        {
            return $"{one} x {two} x {three} = {three*two*one}";
        }
        public static void Looper(int number)
        {
            for(var i=1 ; i<number ; i++)
            {
                Console.WriteLine(i);
            }
        }
        static void Main(string[] args)
        {
            Welcome();
            Welcome();
            Welcome();
            Welcome();
            Welcome();
            Console.WriteLine();

            NameWelcome("Jeff");
            NameWelcome("Alex");
            NameWelcome("James");
            NameWelcome("Ryan");
            NameWelcome("Tony");
            Console.WriteLine();

            Console.WriteLine(NameWelcome2("Jeff"));
            Console.WriteLine(NameWelcome2("Alex"));
            Console.WriteLine(NameWelcome2("James"));
            Console.WriteLine(NameWelcome2("Ryan"));
            Console.WriteLine(NameWelcome2("Tony"));
            Console.WriteLine();

            Console.WriteLine(Multiply(5,6,1));
            Console.WriteLine(Multiply(1,6,5));
            Console.WriteLine(Multiply(5,7,6));
            Console.WriteLine(Multiply(2,7,9));
            Console.WriteLine(Multiply(6,4,3));
            Console.WriteLine();

            Looper(Convert.ToInt32(Console.ReadLine()));
        }
    }
}
